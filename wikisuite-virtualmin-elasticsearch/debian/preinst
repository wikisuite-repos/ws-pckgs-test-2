#!/bin/bash

elastic_memory_swap(){
  mem_total=$(awk '/MemTotal/ {print $2}' /proc/meminfo)
  swap_total=$(awk '/SwapTotal/ {print $2}' /proc/meminfo)
  all_mem=$((mem_total + swap_total))

  min_mem=4194304
  min_mem_h=$((min_mem / 1024))
  
  echo "A ${min_mem_h} MB swap file will be created."

  # Check for btrfs, because it can't host a swap file safely.
  root_fs_type=$(grep -v "^$\\|^\\s*#" /etc/fstab | awk '{print $2 " " $3}' | grep "/ " | cut -d' ' -f2)
  if [ "$root_fs_type" = "btrfs" ]; then
      echo "Error:" \
          "Your root filesystem appears to be running btrfs. It is unsafe to create" \
          "a swap file on a btrfs filesystem. You need to create a swap file manually (on some other filesystem)."
      exit 1
  fi

  # Check for enough space.
  root_fs_avail=$(df /|grep -v Filesystem|awk '{print $4}')
  if [ "$root_fs_avail" -lt $((min_mem + 358400)) ]; then
    root_fs_avail_h=$((root_fs_avail / 1024))
    echo "Error: Root filesystem only has $root_fs_avail_h MB available, which is too small." \
         "You need to add more space to '/'."
    exit 1
  fi

  # Create a new file
  echo "Info: Creating a new swap file"
  if ! dd if=/dev/zero of=/swapfile bs=1024 count=$min_mem; then
    echo "Error: Creating swap file /swapfile failed."
    exit 1
  fi
      
  chmod 0600 /swapfile
  mkswap /swapfile
  if ! swapon /swapfile ; then
    echo "Error: Enabling swap file failed. If this is a VM, it may be prohibited by your provider."
    exit 1
  fi
  
  if ! grep  '\/swapfile\s*swap\s*swap\s*defaults\s*0\s*0'  /etc/fstab; then
    echo "/swapfile          swap            swap    defaults        0 0" >> /etc/fstab
  fi

  echo "Swap created successfully"
}

# If swap does not exist, then create
if [[ $(swapon -s | wc -l) -lt 2 ]] ; then 
  elastic_memory_swap
fi
